package com.promwad.demoapplication.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.promwad.demoapplication.IDataListener;
import com.promwad.demoapplication.IDataService;

public class DataService extends Service {

    private static final String TAG = DataService.class.getSimpleName();

    private LooperThread mLooperThread = new LooperThread();

    private IDataListener mListener = null;

    private String[] mUrls = {
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/8340/production/_101900633_img_5101.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/D160/production/_101900635_img_6467.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/3908/production/_101900641_img_6959.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/11F80/production/_101900637_img_6498.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/16DA0/production/_101900639_img_6719.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/CB71/production/_101918025_img_7004.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/E164/production/_101900775_img_6946.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/F209/production/_101916916_img_7014.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/9A2F/production/_101917493_img_6261.jpg",
            "http://ichef.bbci.co.uk/news/976/cpsprodpb/9344/production/_101900773_mexican_beetles_027.jpg"
    };

    IBinder mServiceBinder = new IDataService.Stub() {
        @Override
        public void refresh() {
            Log.d(TAG, "basicTypes");
            mLooperThread.mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "postDelayed runable");
                    if (mListener != null) {
                        try {
                            mListener.onData(mUrls);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 5000);
        }

        @Override
        public void registerListener(IDataListener listener) throws RemoteException {
            Log.d(TAG, "Registering listener");
            mListener = listener;
        }

        @Override
        public void unregisterListener() {
            Log.d(TAG, "Unregistering listener");
            mListener = null;
        }
    };

    public DataService() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mLooperThread.start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mServiceBinder;
    }

    class LooperThread extends Thread {
        Handler mHandler;
        Looper mLooper;

        public void run() {
            Looper.prepare();
            mLooper = Looper.myLooper();
            mHandler = new Handler() {
                public void handleMessage(Message msg) {
                    Log.d(TAG, "handleMessage");
                    Runnable callback = msg.getCallback();
                    if (callback != null) callback.run();
                }
            };
            Looper.loop();
        }
    }
}
