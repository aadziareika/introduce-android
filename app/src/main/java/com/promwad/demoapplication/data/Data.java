package com.promwad.demoapplication.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aaa on 6/15/18.
 */

public class Data implements Parcelable {

    private String mUrl;

    private int mNumber;

    public Data(String url, int number) {
        mUrl = url;
        mNumber = number;
    }

    public String getUrl() {
        return mUrl;
    }

    public int getNumber() {
        return mNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mNumber);
        parcel.writeString(mUrl);
    }

    public static final Parcelable.Creator<Data> CREATOR
            = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            int number = source.readInt();
            String url = source.readString();
            return new Data(url, number);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}
