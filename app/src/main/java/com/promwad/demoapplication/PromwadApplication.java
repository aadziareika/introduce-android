package com.promwad.demoapplication;

import android.app.Application;
import android.util.Log;

/**
 * PromwadApplication
 *
 * @author Aliaksandr Adziareika
 */
public class PromwadApplication extends Application {

    private static final String TAG = PromwadApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Put extra initialization here");
    }
}
