package com.promwad.demoapplication;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.promwad.demoapplication.data.Data;
import com.promwad.demoapplication.ui.PictureAdapter;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Button mRefresh;

    private PictureAdapter mPictureAdapter = new PictureAdapter();

    private static final String SERVICE_PACKAGE_NAME = "com.promwad.demoapplication";

    private static final String SERVICE_CLASS_NAME
            = "com.promwad.demoapplication.service.DataService";

    private IDataService mService;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = findViewById(R.id.sample_text);
        mRefresh = findViewById(R.id.refresh_button);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mService != null) {
                    try {
                        mService.refresh();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        tv.setText(stringFromJNI());

        RecyclerView recyclerView = findViewById(R.id.list_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false
        );
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mPictureAdapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, LinearLayout.HORIZONTAL)
        );

        mPictureAdapter.setOnClickListener(new PictureAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int postion) {
                Intent intent = new Intent(MainActivity.this,
                        FullscreenActivity.class);
                intent.putExtra(FullscreenActivity.EXTRA,
                        new Data(mPictureAdapter.at(postion), postion));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        Intent intent = new Intent();
        intent.setComponent(new ComponentName(SERVICE_PACKAGE_NAME, SERVICE_CLASS_NAME));
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        unbindService(mConnection);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            Log.d(TAG, "onServiceConnected");
            mService = (IDataService) service;
            try {
                mService.registerListener(mListener);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(TAG, "onServiceDisconnected");
            try {
                mService.unregisterListener();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            mService = null;
        }
    };

    private IDataListener.Stub mListener = new IDataListener.Stub() {
        @Override
        public void onData(String[] data) throws RemoteException {
            Log.d(TAG, String.format("Number of strings passed: " + data.length));
            mPictureAdapter.clear();
            mPictureAdapter.add(data);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPictureAdapter.notifyDataSetChanged();
                }
            });
        }
    };
}
