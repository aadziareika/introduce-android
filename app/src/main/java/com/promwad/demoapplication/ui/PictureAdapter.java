package com.promwad.demoapplication.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.promwad.demoapplication.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * PictureAdapter
 *
 * @author Aliaksandr Adziareika
 */
public class PictureAdapter extends RecyclerView.Adapter<PictureViewHolder> {

    private List<String> mData = new ArrayList<>();

    private OnItemClickListener mClickListener;

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.list_item_view, null);

        v.setClickable(true);
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);

        return new PictureViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureViewHolder holder, int position) {
        holder.loadImageFrom(mData.get(position));
        holder.setOnClickListener(mClickListener);
    }

    public void add(String... data) {
        mData.addAll(Arrays.asList(data));
    }

    public void clear() {
        mData.clear();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOnClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int postion);
    }

    public String at(int position) {
        return mData.get(position);
    }

}
