package com.promwad.demoapplication.ui;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.promwad.demoapplication.R;
import com.squareup.picasso.Picasso;

/**
 * PictureViewHolder
 *
 * @author Aliaksandr Adziareika
 */
public class PictureViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = "PictureViewHolder";

    private ImageView mImageView;

    private PictureAdapter.OnItemClickListener mClickListener;

    public PictureViewHolder(View itemView) {
        super(itemView);
        mImageView = itemView.findViewById(R.id.image_view);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null)
                    mClickListener.onItemClick(getAdapterPosition());
            }
        });
    }

    public void loadImageFrom(String imageUrl) {
        Log.d(TAG, "Loading from: " + imageUrl);
        Picasso.with(itemView.getContext())
                .load(imageUrl)
                .into(mImageView);
    }

    public void setOnClickListener(PictureAdapter.OnItemClickListener listener) {
        mClickListener = listener;
    }

}
