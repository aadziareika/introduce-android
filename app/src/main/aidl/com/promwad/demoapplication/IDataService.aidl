// IDataService.aidl
package com.promwad.demoapplication;

import com.promwad.demoapplication.IDataListener;

// Declare any non-default types here with import statements

interface IDataService {

    void refresh();

    void registerListener(IDataListener listener);

    void unregisterListener();
}
