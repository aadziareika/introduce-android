// IDataListener.aidl
package com.promwad.demoapplication;

interface IDataListener {
    void onData(out String[] aString);
}
